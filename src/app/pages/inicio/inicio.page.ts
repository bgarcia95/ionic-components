import { Component, OnInit } from "@angular/core";

interface Componente {
  icon: string;
  name: string;
  redirectTo: string;
}

@Component({
  selector: "app-inicio",
  templateUrl: "./inicio.page.html",
  styleUrls: ["./inicio.page.scss"],
})
export class InicioPage implements OnInit {
  componentes: Componente[] = [
    {
      icon: "american-football-outline",
      name: "Action Sheet",
      redirectTo: "/action-sheet",
    },
    {
      icon: "alert-circle-outline",
      name: "Alert",
      redirectTo: "/alert",
    },
    {
      icon: "beaker-outline",
      name: "Avatar",
      redirectTo: "/avatar",
    },
    {
      icon: "radio-button-off-outline",
      name: "Buttons",
      redirectTo: "/buttons",
    },
    {
      icon: "card-outline",
      name: "Cards",
      redirectTo: "/cards",
    },
    {
      icon: "checkmark-circle-outline",
      name: "Checkbox",
      redirectTo: "/checkbox",
    },
    {
      icon: "hammer-outline",
      name: "Input",
      redirectTo: "/input",
    },
    {
      icon: "browsers-outline",
      name: "Iframe",
      redirectTo: "/iframe",
    },
  ];

  constructor() {}

  ngOnInit() {}
}
