import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-iframe",
  templateUrl: "./iframe.page.html",
  styleUrls: ["./iframe.page.scss"],
})
export class IframePage implements OnInit {
  constructor() {}

  didShow = false;

  ngOnInit() {
    window.addEventListener("keyboardWillShow", (event) => {
      // Describe your logic which will be run each time when keyboard is about to be shown.
      console.log("Event", event);
      this.didShow = true;
      console.log("Did Show", this.didShow);
    });

    window.addEventListener("keyboardWillHide", () => {
      // Describe your logic which will be run each time when keyboard is about to be closed.
      console.log("Event", event);
      this.didShow = false;
      console.log("Did Show", this.didShow);
    });
  }
}
